﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;



namespace Pojekti
{
    public partial class ConsoleDisplayWindow : Window
    {
        public string Message { get; set; }
        public string ErrorCountText { get; set; }

        public ConsoleDisplayWindow()
        {
            InitializeComponent();
            DataContext = this;
            Loaded += ConsoleDisplayWindow_Loaded;
        }

        private void ConsoleDisplayWindow_Loaded(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Maximized;
        }
    }
    
}
