﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Pojekti
{
    /// <summary>
    /// Interaction logic for Rregulla.xaml
    /// </summary>
    public partial class Rregulla : Window
    {
        public Rregulla()
        {
            InitializeComponent();
            Loaded += Rregulla_Loaded;
        }

        private void Rregulla_Loaded(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Maximized;
        }

        private void Kthehupasbutton_Click(object sender, RoutedEventArgs e)
        {
            FaqjaDyte faqjadyte = new FaqjaDyte();
            faqjadyte.Show();
            this.Close();
        }
    }
}
