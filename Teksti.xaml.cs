﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Pojekti
{
    /// <summary>
    /// Interaction logic for Teksti.xaml
    /// </summary>
    public partial class Teksti : Window
    {
        public Teksti()
        {
            InitializeComponent();
            Loaded += Teksti_Loaded;
        }

        private void Teksti_Loaded(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Maximized;
        }

        private void Kthehupasbutton_Click_1(object sender, RoutedEventArgs e)
        {
            FaqjaDyte faqjadyte = new FaqjaDyte();
            faqjadyte.Show();
            this.Close();
        }
    }
}
