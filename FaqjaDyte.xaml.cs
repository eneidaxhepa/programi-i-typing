﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Pojekti
{
    /// <summary>
    /// Interaction logic for FaqjaDyte.xaml
    /// </summary>
    public partial class FaqjaDyte : Window
    {
        public FaqjaDyte()
        {
            InitializeComponent();
            Loaded += FaqjaDyte_Loaded;
        }

        private void FaqjaDyte_Loaded(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Maximized;
        }

        private void RregullaButton_Click(object sender, RoutedEventArgs e)
        {
           Rregulla rregulla = new Rregulla();
            rregulla.Show();
            this.Close();
        }

        private void TestButton_Click(object sender, RoutedEventArgs e)
        {
            Testi testi = new Testi();
            testi.Show();
            this.Close();
        }

        private void Tekstbutton_Click(object sender, RoutedEventArgs e)
        {
            Teksti teksti = new Teksti();
            teksti.Show();
            this.Close();
        }
    }
}
