﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Pojekti
{
    /// <summary>
    /// Interaction logic for Testi.xaml
    /// </summary>
    public partial class Testi : Window
    {
        private string currentText = "";
        private bool capsLockEnabled = false;
        private bool shiftPressed = false;

        public Testi()
        {
            InitializeComponent();

            InitializeKeyboard();

            Loaded += Testi_Loaded;
            KeyDown += MainWindow_KeyDown;
            KeyUp += MainWindow_KeyUp;
            UpdateOutputTextBox();

        }



        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
            {
                shiftPressed = false;
                UpdateButonatTastjere();
               
            }

        }




        private void Testi_Loaded(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Maximized;
        }




        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.LeftShift:
                case Key.RightShift:
                    shiftPressed = true;
                    UpdateButonatTastjere();
                    e.Handled = true; // Make sure to handle the event
                    break;
            }
                    Button butoniKlikuar = null;

            if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
            {
                shiftPressed = true;
                UpdateButonatTastjere();
                e.Handled = true;
            }

            switch (e.Key)
            {
                case Key.Q:
                    butoniKlikuar = Q;
                    break;
                case Key.W:
                    butoniKlikuar = W;
                    break;
                case Key.E:
                    butoniKlikuar = E;
                    break;
                case Key.R:
                    butoniKlikuar = R;
                    break;
                case Key.T:
                    butoniKlikuar = T;
                    break;
                case Key.Y:
                    butoniKlikuar = Y;
                    break;
                case Key.U:
                    butoniKlikuar = U;
                    break;
                case Key.I:
                    butoniKlikuar = I;
                    break;
                case Key.O:
                    butoniKlikuar = O;
                    break;
                case Key.P:
                    butoniKlikuar = P;
                    break;
                case Key.A:
                    butoniKlikuar = A;
                    break;
                case Key.S:
                    butoniKlikuar = S;
                    break;
                case Key.D:
                    butoniKlikuar = D;
                    break;
                case Key.F:
                    butoniKlikuar = F;
                    break;
                case Key.G:
                    butoniKlikuar = G;
                    break;
                case Key.H:
                    butoniKlikuar = H;
                    break;
                case Key.J:
                    butoniKlikuar = J;
                    break;
                case Key.K:
                    butoniKlikuar = K;
                    break;
                case Key.L:
                    butoniKlikuar = L;
                    break;
                case Key.Z:
                    butoniKlikuar = Z;
                    break;
                case Key.X:
                    butoniKlikuar = X;
                    break;
                case Key.C:
                    butoniKlikuar = C;
                    break;
                case Key.V:
                    butoniKlikuar = V;
                    break;
                case Key.B:
                    butoniKlikuar = B;
                    break;
                case Key.N:
                    butoniKlikuar = N;
                    break;
                case Key.M:
                    butoniKlikuar = M;
                    break;


                case Key.Space:
                    butoniKlikuar = Space;
                    break;

                case Key.Enter:
                    butoniKlikuar = Enter;
                    break;

                case Key.CapsLock:
                    butoniKlikuar = Capslock;
                    break;

                case Key.LeftShift:
                    butoniKlikuar = SHIFT;
                        break;

                case Key.RightShift:
                    butoniKlikuar = SHIFT1;
                    
                    break;
            }

            if (butoniKlikuar != null)
            {
                butoniKlikuar.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
            }
        }


        private string targetText = "Natyra paraqet teresine e karakteristikave me kryesore qe percaktojne thelbin e diçkaje e qe ne kuptimin me te gjere ajo perfshin jo vetem mjedisin ku jetojne njerezit, kafshet dhe bimet, por edhe gjithe boten organike dhe joorganike, fizike ose boten materiale apo gjithesine.Natyra eshte nje term qe mund te perfshije si dukurite e botes fizike ashtu edhe jeten ne pergjithesi.Studimi i totalitetit te natyres eshte nje pjese e madhe e shkences. Edhe pse njerezit jane pjese e natyres, aktiviteti njerezor shpesh kuptohet si nje kategori e veçante.";


        //paragrafi pare qe do i shfaqet perdoruesit 
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {

            Paragraph paragraph = new Paragraph();


            Run initialRun = new Run("Natyra paraqet teresine e karakteristikave me kryesore qe percaktojne thelbin e diçkaje e qe ne kuptimin me te gjere ajo perfshin jo vetem mjedisin ku jetojne njerezit, kafshet dhe bimet, por edhe gjithe boten organike dhe joorganike, fizike ose boten materiale apo gjithesine.Natyra eshte nje term qe mund te perfshije si dukurite e botes fizike ashtu edhe jeten ne pergjithesi.Studimi i totalitetit te natyres eshte nje pjese e madhe e shkences. Edhe pse njerezit jane pjese e natyres, aktiviteti njerezor shpesh kuptohet si nje kategori e veçante.");

            paragraph.Inlines.Add(initialRun);


            OutputRichTextBox.Document.Blocks.Clear();
            OutputRichTextBox.Document.Blocks.Add(paragraph);
        }





        private void InitializeKeyboard()
        {
            if (CustomKeyboardGrid != null && CustomKeyboardGrid.Children != null)
            {
                foreach (UIElement element in CustomKeyboardGrid.Children)
                {
                    if (element is Button button)
                    {
                        string buttonText = button.Content.ToString();
                        if (buttonText != "CapsLock")
                        {
                            button.Click += Button_Click;
                        }
                        if (buttonText == "Shift")
                        {
                            button.Click += SHIFT_Click;
                        }
                    }
                }
            }
        }







        private List<int> foundIndexes = new List<int>();

        private bool shkronjaRradhesKapitale = false;


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button clickedButton = (Button)sender;
            string buttonText = clickedButton.Content.ToString();

            if (currentText == targetText)
            {
                return;
            }

            if (buttonText == "CapsLock")
            {
                capsLockEnabled = !capsLockEnabled;
                UpdateButonatTastjere();
            }
            else if (buttonText == "Enter")
            {
                currentText += Environment.NewLine;
                UpdateOutputTextBox();
            }
            else if (buttonText == "Space")
            {
                currentText += " ";
                UpdateOutputTextBox();
            }
            else if (buttonText == "Shift")
            {
                shkronjaRradhesKapitale = !shkronjaRradhesKapitale;
                UpdateButonatTastjere();
                UpdateOutputTextBox();
            }
            else
            {
                char characterToAdd = buttonText[0];

                if (shiftPressed)
                {
                    characterToAdd = shkronjaRradhesKapitale ? buttonText.ToUpper()[0] : buttonText.ToLower()[0];
                    shkronjaRradhesKapitale = false;
                }
                else
                {
                    characterToAdd = capsLockEnabled ? buttonText.ToUpper()[0] : buttonText.ToLower()[0];
                }

                if (currentText.Length < targetText.Length && targetText[currentText.Length] == characterToAdd)
                {
                    foundIndexes.Add(currentText.Length);
                }
                else
                {
                    return;
                }

                currentText += characterToAdd;
                UpdateOutputTextBox();
            }
        }






        private void UpdateButonatTastjere()
        {
            foreach (UIElement element in CustomKeyboardGrid.Children)
            {
                if (element is Button button)
                {
                    string buttonText = button.Content.ToString();

                    if (buttonText.Length == 1 && char.IsLetter(buttonText[0]))
                    {
                        button.Content = shiftPressed ? buttonText.ToUpper() : buttonText.ToLower();
                    }
                }
            }
        }






        private void Shkronje_Click(object sender, RoutedEventArgs e)
        {
            Button clickedButton = (Button)sender;
            string buttonText = clickedButton.Content.ToString();

            if (buttonText == "CapsLock")
            {
                capsLockEnabled = !capsLockEnabled;
                Capslock_Click();
            }
            else if (buttonText == "Enter")
            {
                currentText += Environment.NewLine;
                UpdateOutputTextBox();
            }
            else if (buttonText == "Space")
            {
                currentText += " ";
                UpdateOutputTextBox();
            }
            else if (buttonText == "Shift")
            {
                currentText += shiftPressed ? buttonText.ToUpper() : buttonText.ToLower();
                UpdateOutputTextBox();
            }
            else
            {
                currentText += capsLockEnabled ? buttonText.ToUpper() : buttonText.ToLower();
                UpdateOutputTextBox();
            }
        }





        private void Capslock_Click()
        {
            capsLockEnabled = !capsLockEnabled;

            foreach (UIElement element in CustomKeyboardGrid.Children)
            {
                if (element is Button button)
                {
                    string buttonText = button.Content.ToString();

                    if (buttonText != "CapsLock" && buttonText.Length == 1)
                    {
                        button.Content = capsLockEnabled ? buttonText.ToUpper() : buttonText.ToLower();
                    }
                }
            }
        }




       private void SHIFT_Click(object sender, RoutedEventArgs e)
{
    shiftPressed = !shiftPressed;

    foreach (UIElement element in CustomKeyboardGrid.Children)
    {
        if (element is Button button)
        {
            string buttonText = button.Content.ToString();

            if (buttonText.Length == 1 && char.IsLetter(buttonText[0]))
            {
                button.Content = shiftPressed ? buttonText.ToUpper() : buttonText.ToLower();
            }
        }
    }
}







        private void Capslock_Click(object sender, RoutedEventArgs e)
        {
           
        capsLockEnabled = !capsLockEnabled;

            foreach (UIElement element in CustomKeyboardGrid.Children)
            {
                if (element is Button button)
                {
                    string buttonText = button.Content.ToString();

                    if (buttonText != "CapsLock" && buttonText.Length == 1)
                    {
                        button.Content = capsLockEnabled ? buttonText.ToUpper() : buttonText.ToLower();
                    }
                }
            }
        }

      






        private void UpdateOutputTextBox()
        {
            OutputRichTextBox.Document.Blocks.Clear();

            Paragraph paragraph = new Paragraph();
            int targetLength = Math.Min(currentText.Length, targetText.Length);

            for (int i = 0; i < targetText.Length; i++)
            {
                SolidColorBrush foreground;

                if (i < targetLength)
                {
                    foreground = (currentText[i] == targetText[i]) ? Brushes.Green : Brushes.Red;
                }
                else
                {
                    foreground = Brushes.Gray;
                }

                Run run = new Run(targetText[i].ToString())
                {
                    Foreground = foreground
                };
                paragraph.Inlines.Add(run);
            }

            OutputRichTextBox.Document.Blocks.Add(paragraph);
        }







        private void UpdateOutputRichTextBox()
        {

            Run run = new Run(currentText);

            OutputRichTextBox.Document.Blocks.Clear();
            OutputRichTextBox.Document.Blocks.Add(new Paragraph(run));

            foreach (int index in foundIndexes)
            {
                if (index < currentText.Length)
                {
                    run = new Run(currentText[index].ToString())
                    {
                        Foreground = Brushes.Green
                    };
                    TextPointer start = OutputRichTextBox.Document.ContentStart.GetPositionAtOffset(index);
                    if (start != null)
                    {
                        TextRange textRange = new TextRange(start, start.GetPositionAtOffset(1));
                        textRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.Green);
                    }
                }
            }
        }







        private int CalculateErrorCount()
        {
            int errorCount = 0;

            for (int i = 0; i < targetText.Length; i++)
            {
                if (i >= currentText.Length || currentText[i] != targetText[i])
                {
                    errorCount++;
                }
            }

            return errorCount;
        }







        private void Shigjete_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(currentText))
            {
                currentText = currentText.Remove(currentText.Length - 1);
                UpdateOutputTextBox();
            }
        }





        private void Enter_Click(object sender, RoutedEventArgs e)
        {
            currentText += Environment.NewLine;
            UpdateOutputTextBox();
        }





        private void PointButton_Click(object sender, RoutedEventArgs e)
        {
            currentText += ".";
            UpdateOutputTextBox();
        }



        private void Space_Click(object sender, RoutedEventArgs e)
        {
            currentText += " ";
            UpdateOutputTextBox();
        }









        internal class KeyboardGrid
        {
            public static IEnumerable<UIElement> Children { get; internal set; }
        }

        private void Number_Click(object sender, RoutedEventArgs e)
        {
            Button clickedButton = (Button)sender;
            string buttonText = clickedButton.Content.ToString();

            if (currentText == targetText)
            {
                return;
            }

            if (buttonText == "CapsLock")
            {

            }
            else if (buttonText == "Enter")
            {

            }
            else if (buttonText == "Space")
            {

            }
            else if (buttonText == "Shift")
            {

            }
            else
            {

                currentText += buttonText;
                UpdateOutputTextBox();
            }
        }






        private void PerfundoButton_Click(object sender, RoutedEventArgs e)
        {
            int errorCount = CalculateErrorCount();
            string message = $"Ju mbaruat se shkruari.\nErrors: {errorCount}";

            ShowConsoleDisplayWindow(message, errorCount);
        }





        private void ShowConsoleDisplayWindow(string message, int errorCount)
        {
            ConsoleDisplayWindow consoleDisplayWindow = new ConsoleDisplayWindow
            {
                Message = message,
                ErrorCountText = $"Gabime: {errorCount}"
            };
            consoleDisplayWindow.Show();
            this.Close();
        }

       

    }

}